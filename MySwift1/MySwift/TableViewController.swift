//
//  TableViewController.swift
//  MySwift
//
//  Created by BLT0008-MCMI on 29/05/15.
//  Copyright (c) 2015 BlissLogix. All rights reserved.
//

import UIKit


class TableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    
        
    @IBOutlet var tableviewName: UITableView!
        var contacts: NSArray!

    override func viewDidLoad() {
        super.viewDidLoad()
         //self.tableviewName.registerClass(UITableViewCell.self, forCellReuseIdentifier: "TableCell")

        var path = NSBundle.mainBundle().pathForResource("contactlist", ofType:"plist")
        
         contacts = NSArray(contentsOfFile: path!)
        
        

        
        // Do any additional setup after loading the view.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
        
        
    }
    
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
  {
    return contacts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableviewName.dequeueReusableCellWithIdentifier("TableCell", forIndexPath: indexPath) as! TableViewCell
        
      
      cell.btnContact.setTitle(self.contacts.objectAtIndex(indexPath.row).valueForKey("Name") as! NSString as String, forState: UIControlState.Normal)
        
        
        
               return cell
    }
    
    func selectRowAtIndexPath(indexPath: NSIndexPath?, animated: Bool, scrollPosition: UITableViewScrollPosition){
        
    }


  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
