//
//  TableViewCell.swift
//  MySwift
//
//  Created by BLT0008-MCMI on 29/05/15.
//  Copyright (c) 2015 BlissLogix. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgThum: UIImageView!
    
    @IBOutlet var imgCall: UIImageView!
    @IBOutlet weak var btnContact: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
