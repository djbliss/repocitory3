//
//  CollectionViewCell.swift
//  MySwift
//
//  Created by BLT0008-MCMI on 28/05/15.
//  Copyright (c) 2015 BlissLogix. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imgCell: UIImageView!
    
    @IBOutlet var lblCellName: UILabel!
    
}
