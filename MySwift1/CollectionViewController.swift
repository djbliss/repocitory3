//
//  CollectionViewController.swift
//  MySwift
//
//  Created by BLT0008-MCMI on 28/05/15.
//  Copyright (c) 2015 BlissLogix. All rights reserved.
//

import UIKit

class CollectionViewController:UIViewController 
   {
    var tabledata :[String]=["MicroCar","SubCompact","CompactCar","MidSize","luxury","SuperCar","Roadster","Grandtourer","MPV","MiniSUV","CompactSUV","MidSizeSUV","FullSizeSUV","Minitruck","MidSizetruck"]
    var tableimage :[String]=["c1.jpg","c2.jpg","c4.jpg","c3.jpg","c5.jpg","c6.jpg","c7.jpg","c8.jpg","c9.jpg","c10.jpg","c11.jpg","c12.jpg","c13.jpg","c14.jpg","c15.jpg"]
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
        // Do any additional setup after loading the view.
    }
   func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
   {
    return tabledata.count;
    
    }
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell: CollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CollectionViewCell
        cell.lblCellName.text = tabledata[indexPath.row]
        cell.imgCell.image = UIImage(named: tableimage[indexPath.row])
        return cell
    }
    
//
//    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
//        println("Cell \(indexPath.row) selected")
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
