//
//  RegisterViewController.swift
//  MySwift
//
//  Created by BLT0008-MCMI on 27/05/15.
//  Copyright (c) 2015 BlissLogix. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController 
{
    
    @IBOutlet var txtName:UITextField!
    @IBOutlet var txtEmail:UITextField!
    @IBOutlet var txtAge:UITextField!
    @IBOutlet var txtPhno:UITextField!
    @IBOutlet var txtpwd:UITextField!
    @IBOutlet var txtrepwd:UITextField!
    
    @IBOutlet var Scroll: UIScrollView!
    @IBAction func btnSubmit(sender: AnyObject)
    {
      if txtName.text=="" || txtEmail.text=="" || txtAge.text=="" || txtPhno.text=="" || txtpwd.text=="" || txtrepwd.text==""
      {
        var alert = UIAlertView(title:"Sorry", message: "Fill all field", delegate: nil, cancelButtonTitle: "ok");
        alert.show()
        
      }
        txtname()
       txtage()
        txtphno()
        txtpassword()
        txtrepassword()
        
    
    }
    
    
    
    @IBAction func txtname()
    {
        var str=txtName.text;
        var length=count(str)
        if length>15
        {
            var alert = UIAlertView(title:"Sorry", message: "Maximum 15 character in Name", delegate: nil, cancelButtonTitle: "ok");
            alert.show()
        }
    }
    
    
    @IBAction func txtage()
    {
        let str=txtAge.text;
        var length=count(str)
        if length>2
        {
            var alert = UIAlertView(title:"Sorry", message: "Enter valid age", delegate: nil, cancelButtonTitle: "ok");
            alert.show()
        }
    }
    @IBAction func txtphno()
    {
        let str=txtPhno.text;
        var length=count(str)
        if length>10
        {
            var alert = UIAlertView(title:"Sorry", message: "Enter 10 digit number", delegate: nil, cancelButtonTitle: "ok");
            alert.show()
        }
    }

    @IBAction func txtpassword()
    {
        let str=txtpwd.text;
        var length=count(str)
        if length>10
        {
            var alert = UIAlertView(title:"Sorry", message: "Maximum 10 character in password", delegate: nil, cancelButtonTitle: "ok");
            alert.show()
        }
    }
    
    @IBAction func txtrepassword()
    {
                if txtpwd.text != txtrepwd.text
        {
            var alert = UIAlertView(title:"Sorry", message: "Mismatch password", delegate: nil, cancelButtonTitle: "ok");
            alert.show()
        }
    }

    @IBAction func begin(sender: AnyObject) {

        var r = view.frame.size.height - 600;
       
        
        self.view.frame.origin.y-=r
    }
    
    
            override func viewDidLoad() {
        super.viewDidLoad()
                /*Scroll.userInteractionEnabled=true
                Scroll.contentSize = CGSizeMake(0, 680)
                Scroll.scrollsToTop=true*/
                Scroll.scrollEnabled = true;
                Scroll.contentSize = CGSizeMake(320, 800);

        // Do any additional setup after loading the view.
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
